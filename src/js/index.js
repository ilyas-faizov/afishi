import $ from "jquery";
import "bootstrap";
import "popper.js";
import "magnific-popup";
import "owl.carousel";

$(document).ready(function(){
	function imgSvg(){
		$('img.img-svg').each(function(){
		  var $img = $(this);
		  var imgClass = $img.attr('class');
		  var imgURL = $img.attr('src');
		  $.get(imgURL, function(data) {
		    var $svg = $(data).find('svg');
		    if(typeof imgClass !== 'undefined') {
		      $svg = $svg.attr('class', imgClass+' replaced-svg');
		    }
		    $svg = $svg.removeAttr('xmlns:a');
		    if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
		      $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
		    }
		    $img.replaceWith($svg);
		  }, 'xml');
		});
	}
	imgSvg();

  $('.advantages-carousel').owlCarousel({
    loop: false,
    margin: 0,
    nav: true,
    dots: false,
    items: 3,
    autoplaySpeed: 3000,
    autoplayTimeout: 4000,
    autoplayHoverPause: true,
    responsive:{
      0:{
        items:1
      },
      768:{
        items:2
      },
      992:{
        items:2
      },
      1200:{
        items:3
      }
    }
  })

	$('.parthners-slider').owlCarousel({
    loop: true,
    margin: 30,
    nav: false,
    dots: false,
    autoWidth: true,
    items: 4,
    //center:true,
    autoplay: true,
    autoplaySpeed: 3000,
    autoplayTimeout: 4000,
    autoplayHoverPause: true,
    responsive:{
      0:{
        items:1
      },
      768:{
        items:2
      },
      992:{
        items:3
      },      
      1200:{
        items:4
      }
  	}
	});

  $('.header-menu').on('click', function(){
    var el = $(this);
      el.toggleClass('show');
      if (el.hasClass('show')) {
        el.find('.hide-menu').removeClass('d-none');
      } else {
        //setTimeout(function(){
          el.find('.hide-menu').addClass('d-none');
        //}, 300);
      }
  });
});